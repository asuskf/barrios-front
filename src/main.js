import Vue from 'vue'
import App from './App.vue'

Vue.config.productionTip = false
import VueGeolocation from 'vue-browser-geolocation';

new Vue({
  render: h => h(App),
}).$mount('#app')
Vue.use(VueGeolocation);
